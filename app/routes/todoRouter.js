// Import thư viện express
const express = require('express');

// Import todoMiddleware
const todoMiddleware = require('../middlewares/todoMiddleware');

// Tạo todoRouter
const todoRouter = express.Router();

// Import todoController
const todoController = require('../controllers/todoController');

// Create new todo
todoRouter.post('/todos', todoMiddleware.postTodoMiddleware, todoController.createTodo);

//get all todo
todoRouter.get('/todos', todoMiddleware.getAllTodoMiddleware, todoController.getAllTodo);

//get todo by id
todoRouter.get('/todos/:todoId', todoMiddleware.getTodoMiddleware, todoController.getTodoById);

//update todo by id
todoRouter.put('/todos/:todoId', todoMiddleware.putTodoMiddleware, todoController.updateTodoById);

//delete todo by id
todoRouter.delete('/todos/:todoId', todoMiddleware.deleteTodoMiddleware, todoController.deleteTodoById);

// get all todo of user
todoRouter.get('/users/:userId/todos', todoController.getTodosOfUsers);

module.exports = { todoRouter };