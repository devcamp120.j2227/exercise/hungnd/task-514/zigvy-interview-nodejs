// Import thư viện mongoose
const mongoose = require('mongoose');

// Khai báo class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

// Khởi tạo commentShema
const commentSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    postId: {
        type: mongoose.Types.ObjectId,
        ref: "post"
    },
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('comment', commentSchema);