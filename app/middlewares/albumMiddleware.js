const getAllAlbumMiddleware = (req, res, next) => {
    console.log("Get All Album!");
    next();
};

const getAlbumMiddleware = (req, res, next) => {
    console.log("Get a Album!");
    next();
};

const postAlbumMiddleware = (req, res, next) => {
    console.log("Create a Album!");
    next();
};

const putAlbumMiddleware = (req, res, next) => {
    console.log("Update a Album!");
    next();
};

const deleteAlbumMiddleware = (req, res, next) => {
    console.log("Delete a Album!");
    next();
};
module.exports  = {
    getAllAlbumMiddleware,
    getAlbumMiddleware,
    postAlbumMiddleware,
    putAlbumMiddleware,
    deleteAlbumMiddleware
}