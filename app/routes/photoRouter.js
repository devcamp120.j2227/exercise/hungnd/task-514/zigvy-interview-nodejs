// Import thư viện express
const express = require('express');

// Import photoMiddleware
const photoMiddleware = require('../middlewares/photoMiddleware');

// Tạo photoRouter
const photoRouter = express.Router();

// Import photoController
const photoController = require('../controllers/photoController');

// Create new photo
photoRouter.post('/photos', photoMiddleware.postPhotoMiddleware, photoController.createPhoto);

//get all photo
photoRouter.get('/photos', photoMiddleware.getAllPhotoMiddleware, photoController.getAllPhoto);

//get photo by id
photoRouter.get('/photos/:photoId', photoMiddleware.getPhotoMiddleware, photoController.getPhotoById);

//update photo by id
photoRouter.put('/photos/:photoId', photoMiddleware.putPhotoMiddleware, photoController.updatePhotoById);

//delete photo by id
photoRouter.delete('/photos/:photoId', photoMiddleware.deletePhotoMiddleware, photoController.deletePhotoById);

// Get photo of users
photoRouter.get('/albums/:albumId/photos', photoController.getPhotosOfAlbums);

module.exports = { photoRouter };