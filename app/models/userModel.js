// Import thư viện mongoose
const mongoose = require('mongoose');

// Khai báo class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

// Khởi tạo geoSchema
const geoSchema = new Schema({
    lat: {
        type: String,
        required: true
    },
    lng: {
        type: String,
        required: true
    }
});

// Khởi tạo addressSchema
const addressSchema = new Schema({
    street:  {
        type: String,
        required: true
    },
    suite:  {
        type: String,
        required: true
    },
    city:  {
        type: String,
        required: true
    },
    zipcode:  {
        type: String,
        required: true
    },
    geo: geoSchema
});

// Khởi tạo companySchema
const companySchema = new Schema({
    name: {
        type: String,
        required: true
    },
    catchPhrase: {
        type: String,
        required: true
    },
    bs: {
        type: String,
        required: true
    }
});

// Khởi tạo userShema
const userSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    name: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true
    },
    address: addressSchema,
    phone: {
        type: String,
        required: true
    },
    website: {
        type: String,
        require: true
    },
    company: companySchema
});

module.exports = mongoose.model("user", userSchema);
