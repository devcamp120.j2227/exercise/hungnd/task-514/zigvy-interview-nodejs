// Import thư viện express
const express = require('express');

// Import CommentMiddleware
const commentMiddleware = require('../middlewares/commentMiddleware');

// Tạo CommentRouter
const commentRouter = express.Router();

// Import CommentController
const commentController = require('../controllers/commentController');

// Create new Comment
commentRouter.post('/comments', commentMiddleware.createCommentMiddleware, commentController.createComment);

//get all Comment
commentRouter.get('/comments', commentMiddleware.getAllCommentMiddleware, commentController.getAllComment);

//get Comment by id
commentRouter.get('/comments/:commentId', commentMiddleware.getCommentMiddleware, commentController.getCommentById);

//update Comment by id
commentRouter.put('/comments/:commentId', commentMiddleware.updateCommentMiddleware, commentController.updateCommentById);

//delete Comment by id
commentRouter.delete('/comments/:commentId', commentMiddleware.deleteCommentMiddleware, commentController.deleteCommentById);

// Get Comment of Post
commentRouter.get('/posts/:postId/comments', commentController.getCommentsOfPosts);

module.exports = { commentRouter };