// Import thư viện express
const express = require('express');

// Import albumMiddleware
const albumMiddleware = require('../middlewares/albumMiddleware');

// Tạo albumRouter
const albumRouter = express.Router();

// Import albumController
const albumController = require('../controllers/albumController');

// Create new album
albumRouter.post('/albums', albumMiddleware.postAlbumMiddleware, albumController.createAlbum);

//get all album
albumRouter.get('/albums', albumMiddleware.getAllAlbumMiddleware, albumController.getAllAlbum);

//get album by id
albumRouter.get('/albums/:albumId', albumMiddleware.getAlbumMiddleware, albumController.getAlbumById);

//update album by id
albumRouter.put('/albums/:albumId', albumMiddleware.putAlbumMiddleware, albumController.updateAlbumById);

//delete album by id
albumRouter.delete('/albums/:albumId', albumMiddleware.deleteAlbumMiddleware, albumController.deleteAlbumById);

// Get album of users
albumRouter.get('/users/:userId/albums', albumController.getAlbumsOfUsers);

module.exports = { albumRouter };