const getAllCommentMiddleware = (req, res, next) => {
    console.log("Get All Comment!");
    next();
};

const getCommentMiddleware = (req, res, next) => {
    console.log("Get a Comment!");
    next();
};

const createCommentMiddleware = (req, res, next) => {
    console.log("Create a Comment!");
    next();
};

const updateCommentMiddleware = (req, res, next) => {
    console.log("Update a Comment!");
    next();
};

const deleteCommentMiddleware = (req, res, next) => {
    console.log("Delete a Comment!");
    next();
};
module.exports  = {
    getAllCommentMiddleware,
    getCommentMiddleware,
    createCommentMiddleware,
    updateCommentMiddleware,
    deleteCommentMiddleware
}