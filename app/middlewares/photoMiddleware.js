const getAllPhotoMiddleware = (req, res, next) => {
    console.log("Get All Photo!");
    next();
};

const getPhotoMiddleware = (req, res, next) => {
    console.log("Get a Photo!");
    next();
};

const postPhotoMiddleware = (req, res, next) => {
    console.log("Create a Photo!");
    next();
};

const putPhotoMiddleware = (req, res, next) => {
    console.log("Update a Photo!");
    next();
};

const deletePhotoMiddleware = (req, res, next) => {
    console.log("Delete a Photo!");
    next();
};
module.exports  = {
    getAllPhotoMiddleware,
    getPhotoMiddleware,
    postPhotoMiddleware,
    putPhotoMiddleware,
    deletePhotoMiddleware
}