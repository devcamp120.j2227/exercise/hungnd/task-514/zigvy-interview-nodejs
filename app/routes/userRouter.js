// Import thư viện express
const express = require('express');

// Import userMiddleware
const userMiddleware = require('../middlewares/userMiddleware');

// Tạo userRouter
const userRouter = express.Router();

// Import userController
const userController = require('../controllers/userController');

// Create new user
userRouter.post('/users', userMiddleware.postUserMiddleware, userController.createUser);

//get all user
userRouter.get('/users', userMiddleware.getAllUserMiddleware, userController.getAllUser);

//get user by id
userRouter.get('/users/:userId', userMiddleware.getUserMiddleware, userController.getUserById);

//update user by id
userRouter.put('/users/:userId', userMiddleware.putUserMiddleware, userController.updateUserById);

//delete user by id
userRouter.delete('/users/:userId', userMiddleware.deleteUserMiddleware, userController.deleteUserById);

module.exports = { userRouter };