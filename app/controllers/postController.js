// Import thư viện mongoose
const mongoose = require('mongoose');

// Import postModel
const postModel = require('../models/postModel');
const userModel = require('../models/userModel');

// Create new post
const createPost = (req, res) => {
    //B1: Thu thập dữ liệu
    const userId = req.body.userId;
    console.log(userId);

    const body = req.body;
    console.log(body);
    //B2: Kiểm tra dữ liệu
    // Kiểm tra userId
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "User ID is not valid!"
        })
    }
    // Kiểm tra title
    if (!body.title) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Title is not valid!"
        })
    }
    // Kiểm tra body
    if (!body.body) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Body is not valid!"
        })
    }
    //B3: Thực hiện tạo mới post
    const newPost = {
        _id: mongoose.Types.ObjectId(),
        userId: userId,
        title: body.title,
        body: body.body
    }
    postModel.create(newPost, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Create new post successfully!",
                "data": data
            })
        }
    })
};

// Get all post
const getAllPost = (req, res) => {
    //B1: Thu thập dữ liệu
    const userId = req.query.userId;
    const condition = {};
    if (userId) {
        condition.userId = userId;
    }
    //B2: Kiểm tra dữ liệu
    //B3: Thực hiện get all post
    postModel
        .find(condition)
        .exec((err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Get all post successfully!",
                "data": data
            })
        }
    })
};

// Get post by id
const getPostById = (req, res) => {
    //B1: Thu thập dữ liệu
    const postId = req.params.postId;
    console.log(postId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "post Id is not valid!"
        })
    }
    // B3: Thực hiện load post theo id
    postModel.findById(postId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Get post by Id successfully!",
                "data": data
            })
        }
    })
};

// Update post by id
const updatePostById = (req, res) => {
    //B1: Thu thập dữ liệu
    const postId = req.params.postId;
    console.log(postId);

    const userId = req.body.userId;
    console.log(userId);

    const body = req.body;
    console.log(body);
    //B2: Kiểm tra dữ liệu
    // KIểm tra post Id
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "post Id is not valid!"
        })
    }
    // Kiểm tra userId
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "User ID is not valid!"
        })
    }
    // Kiểm tra title
    if (!body.title) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Title is not valid!"
        })
    }
    // Kiểm tra body
    if (!body.body) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Body is not valid!"
        })
    }
    //B3: Thực hiện khởi update post
    const updatePost = {
        userId: userId,
        title: body.title,
        body: body.body
    }
    postModel.findByIdAndUpdate(postId, updatePost, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Update post by Id successfully!",
                "data": data
            })
        }
    })
};

// Delete post by id
const deletePostById = (req, res) => {
    //B1: Thu thập dữ liệu
    const postId = req.params.postId;
    console.log(postId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "post Id is not valid!"
        })
    }
    // B3: Thực hiện xóa post theo id
    postModel.findByIdAndDelete(postId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Delete post by Id successfully!",
                "data": data
            })
        }
    })
};

// get Posts Of User
const getPostsOfUser = (req, res) => {
    //B1: Thu thập dữ liệu
    const userId = req.params.userId;
    console.log(userId);
    //B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "User Id is not valid!"
        })
    }
    // Thực hiện cơ sở dữ liệu
    postModel.find({ userId : userId })
        .exec((err, data) => {
            if (err) {
                return res.status(500).json({
                    "status": "Error 500: internal server error",
                    "message": err.message
                })
            }
            else {
                return res.status(201).json({
                    "status": "get post Of User successfully!",
                    "data": data
                })
            }
    })
};

module.exports = {
    createPost,
    getAllPost,
    getPostById,
    updatePostById,
    deletePostById,
    getPostsOfUser
}