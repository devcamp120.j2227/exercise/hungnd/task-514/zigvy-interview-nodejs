// Import thư viện express
const express = require('express');

// Import postMiddleware
const postMiddleware = require('../middlewares/postMiddleware');

// Tạo postRouter
const postRouter = express.Router();

// Import postController
const postController = require('../controllers/postController');

// Create new post
postRouter.post('/posts', postMiddleware.createPostMiddleware, postController.createPost);

//get all post
postRouter.get('/posts', postMiddleware.getAllPostMiddleware, postController.getAllPost);

//get post by id
postRouter.get('/posts/:postId', postMiddleware.getPostMiddleware, postController.getPostById);

//update post by id
postRouter.put('/posts/:postId', postMiddleware.updatePostMiddleware, postController.updatePostById);

//delete post by id
postRouter.delete('/posts/:postId', postMiddleware.deletePostMiddleware, postController.deletePostById);

// Get post of users
postRouter.get('/users/:userId/posts', postController.getPostsOfUser);

module.exports = { postRouter };