const getAllPostMiddleware = (req, res, next) => {
    console.log("Get All Post!");
    next();
};

const getPostMiddleware = (req, res, next) => {
    console.log("Get a Post!");
    next();
};

const createPostMiddleware = (req, res, next) => {
    console.log("Create a Post!");
    next();
};

const updatePostMiddleware = (req, res, next) => {
    console.log("Update a Post!");
    next();
};

const deletePostMiddleware = (req, res, next) => {
    console.log("Delete a Post!");
    next();
};
module.exports  = {
    getAllPostMiddleware,
    getPostMiddleware,
    createPostMiddleware,
    updatePostMiddleware,
    deletePostMiddleware
}