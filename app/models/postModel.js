// Import thư viện mongoose
const mongoose = require('mongoose');

// Khai báo class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

// Khởi tạo postSchema
const postSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    userId: {
        type: mongoose.Types.ObjectId,
        ref: "user"
    },
    title: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('post', postSchema);