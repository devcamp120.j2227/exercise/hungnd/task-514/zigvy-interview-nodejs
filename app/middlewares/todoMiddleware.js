const getAllTodoMiddleware = (req, res, next) => {
    console.log("Get All Todo!");
    next();
};

const getTodoMiddleware = (req, res, next) => {
    console.log("Get a Todo!");
    next();
};

const postTodoMiddleware = (req, res, next) => {
    console.log("Create a Todo!");
    next();
};

const putTodoMiddleware = (req, res, next) => {
    console.log("Update a Todo!");
    next();
};

const deleteTodoMiddleware = (req, res, next) => {
    console.log("Delete a Todo!");
    next();
};
module.exports  = {
    getAllTodoMiddleware,
    getTodoMiddleware,
    postTodoMiddleware,
    putTodoMiddleware,
    deleteTodoMiddleware
}