// Import thư viện express
const express = require('express');

// Khởi tạo 1 app express
const app = express();

// Import thư viện mongoose
const mongoose = require('mongoose');

// Khai báo port sử dụng
const port = 8000;

// Khai báo để sử dụng body json
app.use(express.json());

// Khai báo để sử dụng tiếng việt UTF-8
app.use(express.urlencoded({
    extended: true
}));

// Kết nối với mongodb
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Zigvy-Interview", (error) => {
    if (error) throw error;
    console.log('Successfully connected');
});

// Import userRouter
const { userRouter } = require('./app/routes/userRouter');
app.use('/api', userRouter);

// Import postRouter
const { postRouter } = require('./app/routes/postRouter');
app.use('/api', postRouter);

// Import commentRouter 
const { commentRouter } = require('./app/routes/commentRouter');
app.use('/api', commentRouter);

const { albumRouter } = require('./app/routes/albumRouter');
app.use('/api', albumRouter);

const { photoRouter } = require('./app/routes/photoRouter');
app.use('/api', photoRouter);

const { todoRouter } = require('./app/routes/todoRouter');
app.use('/api', todoRouter);

app.use((request, response, next) => {
    console.log("Time", new Date());
    next();
}, (request, response, next) => {
    console.log("Request method: ", request.method);
    next();
});

// Khởi động server
app.listen(port, () => {
    console.log(`App running on port ${port}`);
});